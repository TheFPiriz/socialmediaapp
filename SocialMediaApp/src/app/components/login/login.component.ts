import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgForm } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthorityService } from 'src/app/services/authority.service';
import { RegisterComponent } from '../register/register.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  subs: Subscription[] = [];
  constructor(private authService:AuthorityService,
    private afAuth: AngularFireAuth,
    private router: Router,
    // para comunicarme con el modelo
    private matDialog: MatDialog) { }

  ngOnInit(): void {
    this.subs.push(this.authService.UserData.subscribe(user => {
      if(user){
        this.router.navigateByUrl('/').then();
      }
    }))
  }

  ngOnDestroy():void{
    this.subs.map(s=>s.unsubscribe());
  }

  login(form: NgForm): void{
    const{email, password} = form.value;
    if(!form.valid){
      return;
    }
    this.authService.SignIn(email, password);
    form.resetForm();
  }

  openRegister():void{
    const dialogRef = this.matDialog.open(RegisterComponent, {
      role: 'dialog',
      height: '480px',
      width: '480px'
    });

    dialogRef.afterClosed().subscribe(result =>{
      const{fname, lname, email, password, avatar} = result;

      if(result != undefined){
        this.authService.SignUp(email, password, fname, lname, avatar);
      }else{
        return RegisterComponent;
      }
    });
  }
}
