import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AuthorityService, UserData} from '../../services/authority.service';
import {PostService} from '../../services/post.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  images: any[] = [
    'https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_960_720.jpg',
    'https://dam.tbg.com.mx/content/dam/editorialTelevisa/mexico/natgeo/mx/traveler/fotogalerias/16/02/11/Monument-valley.jpg.imgo.jpg',
    'https://storage.googleapis.com/afs-prod/media/26ce3075727548a28db609d558dc95ce/800.jpeg',
    'https://dam.cocinafacil.com.mx/wp-content/uploads/2020/04/chow-mein.jpg',
    'https://concepto.de/wp-content/uploads/2018/08/Londres-e1533855310803.jpg',
    'https://miviaje.com/wp-content/uploads/2018/11/playa-paraiso-mexico.jpg'
  ];

  posts: any[] = [];
  user: UserData;
  subs: Subscription[] = [];

  constructor(private postService: PostService,
    private AuthorityService: AuthorityService) { 

  }

  ngOnInit():void {
    this.subs.push(
      this.postService.getAllPosts().subscribe(posts =>{this.posts =posts;})
        // console.log(posts);
  );
    this.subs.push(
      this.AuthorityService.CurrectUser().subscribe(user =>{
        this.user = user;
        // console.log(user)
      })
    )
  }
  postMessage(form: NgForm): void{
    const{message} = form.value;
    this.postService.postMessage(message, `${this.user.firstName} ${this.user.lastName}`, {
      avatar: this.user.avatar,
      lastName: this.user.lastName,
      firstName:this.user.firstName
    });

    form.resetForm();
  }

  logOut():void{
    this.AuthorityService.Logout();
  }

  ngOnDestroy():void{
    this.subs.map(s => Subscription => s.unsubscribe());
  }

  GoHome():void{
    this.AuthorityService.GoHome();
  }

  GoFriend():void{
    this.AuthorityService.GoFriend();
  }
}
