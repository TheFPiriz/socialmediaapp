import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthorityService, UserData } from 'src/app/services/authority.service';
import { FriendService } from 'src/app/services/friend.service';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class FriendComponent implements OnInit{

  posts: any[] = [];
  user:UserData;
  users: any[] = [];
  subs: Subscription[] = [];

  constructor(private postService: PostService,
    private AuthorityService: AuthorityService,
    private FriendService: FriendService) { 

  }

    

    async ngOnInit(): Promise<void> {
      this.subs.push(
        this.postService.getAllPosts().subscribe(async(posts) =>{this.posts =posts;})
          // console.log(posts);    
    );
      this.subs.push(
        this.AuthorityService.CurrectUser().subscribe(user =>{this.user = user;})
          // console.log(user)
    );
      this.subs.push(this.FriendService.getAllUsers().subscribe(async(users) =>{this.users =users;})
          // console.log(user);
        
    )
    }

    
  
    logOut():void{
      this.AuthorityService.Logout();
    }
  
    GoHome():void{
      this.AuthorityService.GoHome();
    }
  
    GoFriend():void{
      this.AuthorityService.GoFriend();
    }

}
