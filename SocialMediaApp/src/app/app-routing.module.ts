import { HomeComponent } from './components/home/home.component';
import { HoneyGuard } from './guards/honey.guard';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FriendComponent } from './components/friend/friend.component';
import { RegisterComponent } from './components/register/register.component';

/*Voy a hacer lazy loading que consiste en retrasar la 
carga o inicializacion hasta que se utilice*/
const routes: Routes = [
  {
    //De esta manera, solo cargara este modulo en caso de que se acceda a el
    path: '', 
    component: HomeComponent,
    //loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule),
    /*Los guards se van a usar para proteger las rutas y que no entren usuarios
    que no estan logueados*/
    canActivate:[HoneyGuard]
  },
  {
    //Redireccion a login y va al componente de login
    path: 'login',
    component: LoginComponent
  },
  {
    //Redireccion a la pantalla donde se listan los usuarios
    path: 'friend',
    component: FriendComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
