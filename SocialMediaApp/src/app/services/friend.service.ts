import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import {User} from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FriendService {
  currentUser: User;

  constructor(private afs: AngularFirestore,
     private afAuth: AngularFireAuth) {

      this.afAuth.authState.subscribe(user => this.currentUser = user);
    }

    getAllUsers(): Observable<any>{
      return this.afs.collection<any>('users', ref => ref)
      .snapshotChanges()
      .pipe(
        map( actions =>{
          return actions.map(item => {
            return{
              id: item.payload.doc.id,
              ...item.payload.doc.data(),
            };
          });
        })
      )
    }
}
