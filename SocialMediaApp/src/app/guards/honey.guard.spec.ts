import { TestBed } from '@angular/core/testing';

import { HoneyGuard } from './honey.guard';

describe('HoneyGuard', () => {
  let guard: HoneyGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HoneyGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
