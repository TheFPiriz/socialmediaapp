import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthorityService } from '../services/authority.service';

@Injectable({
  providedIn: 'root'
})
export class HoneyGuard implements CanActivate {
  constructor(private router:Router,
    private afAuth: AngularFireAuth,
    private authService: AuthorityService){

    }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.UserData
    .pipe(
      map(user => user != null),
      tap(value => {
        if(!value){
          this.router.navigateByUrl('/login').then();
          return value;
        }else{
          return value;
        }
      })
    );
  }
  
}
