// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: 'AIzaSyD4LORaFucMrBaXFZk9hmoGXmoKMXRGz20',
    authDomain: 'socialmediaapp-1e257.firebaseapp.com',
    projectId: 'socialmediaapp-1e257',
    storageBucket: 'socialmediaapp-1e257.appspot.com',
    messagingSenderId: '387559243214',
    appId: '1:387559243214:web:76d3156293e12f446cbf9f'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
